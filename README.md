#  jberrettamoreno.com Deployment

  
## Using docker-compose

 1.  Go to the **docker-compose** folder
 2. Then run one of the three main (One per branch) modes to deploy:
	 - **master** version: `docker-compose up -d`
	 - **development** version: `docker-compose -f docker-compose.yml -f docker-compose.dev.yml up -d`
	 - **staging** version: `docker-compose -f docker-compose.yml -f docker-compose.staging.yml up -d`


## How to test the ui alone 

 1. Run the services separately using this command (replacing **MODE** with **dev**, **staging** or **prod**):

		 docker-compose -f docker-compose.yml -f docker-compose.**MODE**.yml up
		     -d redis mongo elasticsearch api
		     
2. Then download the ui code from https://gitlab.com/cocheok/jberrettamoreno and follow their deployment steps.


## How to test the api alone 

 1. Run the services separately using this command (replacing **MODE** with **dev**, **staging** or **prod**):

		 docker-compose -f docker-compose.yml -f docker-compose.**MODE**.yml up
		     -d redis mongo elasticsearch


2. Then download the api code from https://gitlab.com/cocheok/jberrettamoreno-api and follow their deployment steps.

## How to run stress tests

1.  Go to the **artillery** folder
2.  In every case there are 2 variables *duration* (execution duration) and *arrivalRate* (Users accesing per second). Example: with a duration of 5 and an arrivalRate of 1 it means that the first second there are 1 user, on the following second arrives other user so there are 2 users, on the third 3 users and so on... so the total amount of users on that period of 5 seconds are 15 (1+2+3+4+5).
3.	Stress socket or api:
	 - **socket**: `artillery run socket.yml -o socket.json`
	 - **api**: `artillery run api.yml -o api.json`
4.	Read results:
	 - **socket**: `artillery report socket.json`
	 - **api**: `artillery report api.json`


## Initialize Elasticsearch 
1. When elasticsearch starts, go to `scripts` folder and run the following http request to create the elasticsearch index:

			curl -X PUT -d @indexBody.json http://0.0.0.0:9200/connection --header "Content-Type:application/json"

 2. Then go to http://localhost to see the application running
 

## Deploy with kubernetes in Google Cloud

  

 1. Create a simple kuberntes cluster
 2. Deploy elasticsearch with the from marketplace in the created cluster
	 - Add the name elasticsearch as a servicename
	 - Initialize the elasticsearch service
3. Access to the cluster upload the k8s folder content to the cluster
4. Run the following commands to create mongo and redis:

	    kubectl create -f mongo-controller.yaml
	    kubectl create -f mongo-service.yaml
	    kubectl create -f redis-controller.yaml
	    kubectl create -f redis-service.yaml


5. Wait to the mongo and readis pods and services creation, they must be in Running status. use this command to check:


		kubectl get pods
		kubectl get services

6. When the mongo and readis are running start the api with the following command:

	    kubectl create -f api-controller.yaml
	    kubectl create -f api-service.yaml

7. Wait again to the api pod creation. When the api is running start the web client with the following command:

	    kubectl create -f web-controller.yaml

8.	 Wait again to the web pod creation. When the web is running start create the web client service with the following command:

		  kubectl create -f web-service.yaml

9. Wait for the web client service creation and get the external IP with the following command:

		kubectl get services

10. Put the external IP in your web browser and enjoy the application!