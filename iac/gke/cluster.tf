resource "google_container_cluster" "jberrettamoreno-test" {
  name               = "jberrettamoreno-test"
  location           = "us-central1-a"
  initial_node_count = 3

  master_auth {
    username = "${var.username}"
    password = "${var.password}"
  }
  
  node_config {
    oauth_scopes = [
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
    ]

  }

}


#####################################################################
# Output for K8S
#####################################################################
output "client_certificate" {
  value     = "${google_container_cluster.jberrettamoreno-test.master_auth.0.client_certificate}"
  sensitive = true
}

output "client_key" {
  value     = "${google_container_cluster.jberrettamoreno-test.master_auth.0.client_key}"
  sensitive = true
}

output "cluster_ca_certificate" {
  value     = "${google_container_cluster.jberrettamoreno-test.master_auth.0.cluster_ca_certificate}"
  sensitive = true
}

output "host" {
  value     = "${google_container_cluster.jberrettamoreno-test.endpoint}"
  sensitive = true
}