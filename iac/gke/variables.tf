
variable "region" {
  default = "us-central1"
}

variable "region_zone" {
  default = "us-central1-c"
}

variable "project_name" {
  description = "gpc google project"
  default = "jberrettamoreno"
}
variable "username" {}
variable "password" {}